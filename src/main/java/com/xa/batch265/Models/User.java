package com.xa.batch265.Models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long Id;
	
	@Column(name = "biodata_id")
	private Long BiodataId;
	
	@Column(name = "role_id")
	private Long RoleId;
	
	@Column(name = "email", length = 100)
	private String Email;
	
	@Column(name = "password")
	private String Password;
	
	@Column(name = "login_attempt")
	private Integer LoginAttempt;
	
	@Column(name = "is_locked")
	private Boolean IsLocked;
	
	@Column(name = "last_login")
	private LocalDateTime LastLogin;
	
	@Column(name = "created_by")
	private Long CreatedBy;
	
	@Column(name = "created_on")
	private LocalDateTime CreatedOn;
	
	@Column(name = "modified_by")
	private Long ModifiedBy;
	
	@Column(name = "modified_on")
	private LocalDateTime ModifiedOn;
	
	@Column(name = "deleted_by")
	private Long DeletedBy;
	
	@Column(name = "deleted_on")
	private LocalDateTime DeletedOn;
	
	@Column(name = "is_delete", columnDefinition = "boolean default false")
	private Boolean IsDelete;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getBiodataId() {
		return BiodataId;
	}

	public void setBiodataId(Long biodataId) {
		BiodataId = biodataId;
	}

	public Long getRoleId() {
		return RoleId;
	}

	public void setRoleId(Long roleId) {
		RoleId = roleId;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public Integer getLoginAttempt() {
		return LoginAttempt;
	}

	public void setLoginAttempt(Integer loginAttempt) {
		LoginAttempt = loginAttempt;
	}

	public Boolean isIsLocked() {
		return IsLocked;
	}

	public void setIsLocked(Boolean isLocked) {
		IsLocked = isLocked;
	}

	public LocalDateTime getLastLogin() {
		return LastLogin;
	}

	public void setLastLogin(LocalDateTime lastLogin) {
		LastLogin = lastLogin;
	}

	public Long getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Long createdBy) {
		CreatedBy = createdBy;
	}

	public LocalDateTime getCreatedOn() {
		return CreatedOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		CreatedOn = createdOn;
	}

	public Long getModifiedBy() {
		return ModifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		ModifiedBy = modifiedBy;
	}

	public LocalDateTime getModifiedOn() {
		return ModifiedOn;
	}

	public void setModifiedOn(LocalDateTime modifiedOn) {
		ModifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return DeletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		DeletedBy = deletedBy;
	}

	public LocalDateTime getDeletedOn() {
		return DeletedOn;
	}

	public void setDeletedOn(LocalDateTime deletedOn) {
		DeletedOn = deletedOn;
	}

	public Boolean isIsDelete() {
		return IsDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		IsDelete = isDelete;
	}

	
	
	
}
