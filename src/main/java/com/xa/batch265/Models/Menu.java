package com.xa.batch265.Models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_menu")
public class Menu {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long Id;
	
	@Column(name = "name", length = 20)
	private String Name;
	
	@Column(name = "url", length = 50)
	private String Url;
	
	@Column(name = "parent_id")
	private Long ParentId;
	
	@Column(name = "big_icon", length = 100)
	private String BigIcon;
	
	@Column(name = "small_icon", length = 100)
	private String SmallIcon;
	
	@Column(name = "created_by")
	private Long CreatedBy;
	
	@Column(name = "created_on")
	private LocalDateTime CreatedOn;
	
	@Column(name = "modified_by")
	private Long ModifiedBy;
	
	@Column(name = "modified_on")
	private LocalDateTime ModifiedOn;
	
	@Column(name = "deleted_by")
	private Long DeletedBy;
	
	@Column(name = "deleted_on")
	private LocalDateTime DeletedOn;
	
	@Column(name = "is_delete" ,columnDefinition = "boolean default false")
	private Boolean IsDelete;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

	public Long getParentId() {
		return ParentId;
	}

	public void setParentId(Long parentId) {
		ParentId = parentId;
	}

	public String getBigIcon() {
		return BigIcon;
	}

	public void setBigIcon(String bigIcon) {
		BigIcon = bigIcon;
	}

	public String getSmallIcon() {
		return SmallIcon;
	}

	public void setSmallIcon(String smallIcon) {
		SmallIcon = smallIcon;
	}

	public Long getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Long createdBy) {
		CreatedBy = createdBy;
	}

	public LocalDateTime getCreatedOn() {
		return CreatedOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		CreatedOn = createdOn;
	}

	public Long getModifiedBy() {
		return ModifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		ModifiedBy = modifiedBy;
	}

	public LocalDateTime getModifiedOn() {
		return ModifiedOn;
	}

	public void setModifiedOn(LocalDateTime modifiedOn) {
		ModifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return DeletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		DeletedBy = deletedBy;
	}

	public LocalDateTime getDeletedOn() {
		return DeletedOn;
	}

	public void setDeletedOn(LocalDateTime deletedOn) {
		DeletedOn = deletedOn;
	}

	public Boolean isIsDelete() {
		return IsDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		IsDelete = isDelete;
	}
	
	
}
