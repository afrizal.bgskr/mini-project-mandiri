package com.xa.batch265.Controller;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xa.batch265.Models.Menu;
import com.xa.batch265.Models.Role;
import com.xa.batch265.Reporsitories.MenuRepo;
import com.xa.batch265.Reporsitories.MenuRoleRepo;
import com.xa.batch265.Reporsitories.RoleRepo;

@Controller
@RequestMapping(value="/")
public class LandingPageController {
	
	@Autowired
	private MenuRoleRepo menuRoleRepo;
	
	@Autowired
	private RoleRepo roleRepo;
	
	@Autowired
	private MenuRepo menuRepo;
	
	@GetMapping(value="")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/landing page/landing-page");
		Long roleId = this.roleRepo.findIdByName("umum") ;
		Collection<Long> menuId = this.menuRoleRepo.findByRoleId(roleId);
		List<Menu> menu = this.menuRepo.findByIdList(menuId);
		view.addObject("menu", menu);
		return view;
	}
	
	@GetMapping(value="beranda")
	public ModelAndView beranda() {
		ModelAndView view = new ModelAndView("/landing page/index");
		return view;
	}
	@GetMapping(value="menu/{roleId}")
	public ModelAndView menu(@PathVariable("roleId") Long roleId) {
		ModelAndView view = new ModelAndView("/landing page/menu");
		Collection<Long> menuId = this.menuRoleRepo.findByRoleId(roleId);
		List<Menu> menu = this.menuRepo.findByIdList(menuId);
		view.addObject("menu", menu);
		return view;
	}
}
