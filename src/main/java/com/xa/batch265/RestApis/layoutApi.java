package com.xa.batch265.RestApis;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.batch265.Models.Biodata;
import com.xa.batch265.Models.Menu;
import com.xa.batch265.Models.User;
import com.xa.batch265.Reporsitories.BiodataRepo;
import com.xa.batch265.Reporsitories.MenuRepo;
import com.xa.batch265.Reporsitories.MenuRoleRepo;

@RestController
@RequestMapping(value="api/layout/")
@CrossOrigin("*")
public class layoutApi {
	
	@Autowired
	private MenuRepo menuRepo;
	
	@Autowired
	private MenuRoleRepo menuRoleRepo;
	
	@Autowired
	private BiodataRepo biodataRepo;
	
	@GetMapping(value="username/{bioId}")
	public ResponseEntity<Optional<Biodata>> username(@PathVariable("bioId") Long bioId){
		Optional<Biodata> bio = this.biodataRepo.findById(bioId);
		if(bio.isPresent()) {
			ResponseEntity rest= new ResponseEntity<>(bio,HttpStatus.OK);
			return rest;
		}
		else {
			ResponseEntity rest= new ResponseEntity<>(HttpStatus.NO_CONTENT);
			return rest;
		}
		
		
		
	}
	
	@GetMapping(value="menu/{roleId}")
	public ResponseEntity<List<Menu>> menu(@PathVariable("roleId") Long roleId){
		
		try {
			Collection<Long> menuId = this.menuRoleRepo.findByRoleId(roleId);
			List<Menu> menu = this.menuRepo.findByIdList(menuId);
			if(menu.isEmpty()) {
				ResponseEntity rest = new ResponseEntity<>(HttpStatus.NO_CONTENT);
				return rest;
			}
			else {
				ResponseEntity rest = new ResponseEntity<>(menu,HttpStatus.OK);
				return rest;
			}
			
		}catch(Exception e){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	
	}
}
