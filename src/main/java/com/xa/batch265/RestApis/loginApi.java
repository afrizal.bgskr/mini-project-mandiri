package com.xa.batch265.RestApis;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.batch265.Models.User;
import com.xa.batch265.Reporsitories.UserRepo;

@RestController
@RequestMapping(value="/api/login/")
@CrossOrigin("*")
public class loginApi {
	
	@Autowired
	private UserRepo userRepo;
	
	@GetMapping(value="email/{email}")
	public ResponseEntity<Boolean> checkEmail(@PathVariable("email") String email){
		Boolean hasil = false;
		try {
			Optional<User> emailUser = this.userRepo.findByEmail(email);
			if(emailUser.isPresent()) {
				
				hasil = true;
				ResponseEntity rest = new ResponseEntity<>(hasil,HttpStatus.OK);
				return rest;
			}
			else {
				ResponseEntity rest = new ResponseEntity<>(hasil,HttpStatus.OK);
				return rest;
			}
			
		}catch(Exception e){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
				
	}
	
	@GetMapping(value="password/{email}/{password}")
	public ResponseEntity<Optional<User>> checkPassword(@PathVariable("email") String email,@PathVariable("password") String password){
		try {
			Optional<User> user = this.userRepo.getLogin(email, password);
			
			if(user.isPresent()) {
				User saveUser= user.get();
				saveUser.setLastLogin(LocalDateTime.now());
				if(saveUser.getLoginAttempt() == null) {
					saveUser.setLoginAttempt(0);
					saveUser.setLoginAttempt(saveUser.getLoginAttempt()+1);
				}
				else {
					saveUser.setLoginAttempt(saveUser.getLoginAttempt()+1);
				}
				
				this.userRepo.save(saveUser);
				ResponseEntity rest = new ResponseEntity<>(user,HttpStatus.OK);
				return rest;
			}
			else {
				ResponseEntity rest = new ResponseEntity<>(HttpStatus.NO_CONTENT);
				return rest;
			}
			
		}catch(Exception e){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
				
	}
	
}
