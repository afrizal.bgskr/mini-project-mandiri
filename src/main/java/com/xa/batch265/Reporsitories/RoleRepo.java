package com.xa.batch265.Reporsitories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xa.batch265.Models.Role;

public interface RoleRepo extends JpaRepository<Role, Long>{
	@Query(value = "SELECT r.id FROM m_role r WHERE name = ?1 ",nativeQuery = true)
	Long findIdByName(String name);
	
	
}
