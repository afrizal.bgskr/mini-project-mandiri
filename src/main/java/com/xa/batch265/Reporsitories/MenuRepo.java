package com.xa.batch265.Reporsitories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.xa.batch265.Models.Menu;

public interface MenuRepo extends JpaRepository<Menu, Long> {
	@Query(value="SELECT * FROM m_menu m WHERE m.id IN :ids ORDER BY parent_id ASC ",nativeQuery = true)
	List<Menu> findByIdList(@Param("ids") Collection<Long> ids); 
}
