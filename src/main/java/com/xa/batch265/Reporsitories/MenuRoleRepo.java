package com.xa.batch265.Reporsitories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xa.batch265.Models.MenuRole;


public interface MenuRoleRepo extends JpaRepository<MenuRole, Long>{

	@Query(value="SELECT mr.menu_id FROM m_menu_role mr Where mr.role_id = ?1" ,nativeQuery = true)
	Collection<Long> findByRoleId(Long role_id);
	

	
}
