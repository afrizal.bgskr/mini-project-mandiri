package com.xa.batch265.Reporsitories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xa.batch265.Models.User;

public interface UserRepo extends JpaRepository<User, Long>{

	@Query(value = "SELECT * FROM m_user u WHERE u.email= ?1",nativeQuery = true)
	Optional<User> findByEmail(String email); 
	
	@Query(value = "SELECT * FROM m_user u WHERE u.email= ?1 and u.password = ?2",nativeQuery = true)
	Optional<User> getLogin(String email,String password); 
}
