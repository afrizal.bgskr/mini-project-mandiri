package com.xa.batch265.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/test/")
public class testController {

	@GetMapping(value="medic")
	public ModelAndView medic() {
		ModelAndView view = new ModelAndView("/test/index");
		return view;
	}
}
